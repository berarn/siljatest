<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'proba1' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'p#9Br=/Ax}y|K{mw)I %?}_/_Fa..a3s/3/-kdQy 7eB~jJ$p[/?kJ^A`DLsCdgE' );
define( 'SECURE_AUTH_KEY',  ']_y_I J4LGcA9}Ht0,#HC6]/pu7toL^X;e]jP5ne]zvUXR_z^W,8Yiluw86 2o-0' );
define( 'LOGGED_IN_KEY',    '~r%N7HX4hM;}X0Fj(tJpHH8.S! WeRn$Y>.Cp9OIv.PZDO`!0GPhSZW5.hRQv1V:' );
define( 'NONCE_KEY',        '{zhDYP7;5G3C7pWmNAd%BK{=iS&ZI2<}Clwm{M*gG4Zn!7knSVV2>OF(O#H!EPSK' );
define( 'AUTH_SALT',        'ck/P>{3mWlo7U9p47Q&r3LykMK6#H1??@I@Szbg3N2vMzWWLlOfETpb52u=D95r1' );
define( 'SECURE_AUTH_SALT', 't^t9OH.b>ivC&oEXm[6y)<nSi[H>}2BnB^xU?S=Ae-AN]1/M$dJs[^ |!<=j9_zi' );
define( 'LOGGED_IN_SALT',   '}3ERc%LnS#G/$eQg)!{}9lw+*M7# tDD){rDRr^Hp |$MGqcv*LZ(XSXIg0eo,%J' );
define( 'NONCE_SALT',       'iZ@`*:rfPBmuUG3lp@Vg(BXZMm/op7|_hZvYQ&r.Kjn~^iP:vA85o@F0){UK|=G~' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
