<!doctype html>
<html lang="zxx">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

       <?php wp_head();?>

        <!-- Title -->
        <title>Viscous - Cleaning Services HTML Template</title>

        <!-- Favicon Link -->
        <link rel="icon" type="image/png" href="assets/img/favicon.png">      
    </head>
    <body>

        <!-- Pre Loader Start -->
        <div class="loader-content">
            <div class="d-table">
                <div class="d-table-cell">
                    <div id="loading-center">
                        <div id="loading-center-absolute">
                            <div class="object" id="object_one"></div>
                            <div class="object" id="object_two"></div>
                            <div class="object" id="object_three"></div>
                            <div class="object" id="object_four"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Pre Loader End -->

        <!-- Header Section Start -->
        <div class="header-section">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-8 col-md-12">
                        <div class="header-widget">
                            <ul>
                                <li>
                                    <i class="icofont-clock-time"></i>
                                    Sun - Thu : 10:00AM - 06:00PM
                                </li>
                                <li>
                                    <i class="icofont-location-pin"></i>
                                    28/A Street, New York City
                                </li>
                                <li>
                                    <i class="icofont-phone"></i>
                                    <a href="tel:+880123456789">
                                        +88 0123 456 789
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="header-social text-right">
                            <ul>
                                <li>
                                    <a href="#"><i class="icofont-facebook"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="icofont-twitter"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="icofont-linkedin"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="icofont-instagram"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>              
                </div>
            </div>
        </div>
        <!-- Header Section End -->