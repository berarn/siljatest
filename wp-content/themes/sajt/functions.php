 <?php

    function load_stylesheets()
    {

        wp_register_style(
            'bootstrap',
            get_template_directory_uri() . '/assets/css/bootstrap.min.css',
            array(),
            1,
            'all'
        );
        wp_enqueue_style('bootstrap');

        wp_register_style(
            'carousel',
            get_template_directory_uri() . '/assets/css/owl.carousel.min.css',
            array(),
            1,
            'all'
        );
        wp_enqueue_style('carousel');

        wp_register_style(
            'default',
            get_template_directory_uri() . '/assets/css/owl.theme.default.min.css',
            array(),
            1,
            'all'
        );
        wp_enqueue_style('default');

        wp_register_style(
            'slick',
            get_template_directory_uri() . '/assets/css/slick.css',
            array(),
            1,
            'all'
        );
        wp_enqueue_style('slick');

        wp_register_style(
            'slick-theme',
            get_template_directory_uri() . '/assets/css/slick-theme.css',
            array(),
            1,
            'all'
        );
        wp_enqueue_style('slick-theme');

        wp_register_style(
            'icofont',
            get_template_directory_uri() . '/assets/css/icofont.min.css',
            array(),
            1,
            'all'
        );
        wp_enqueue_style('icofont');

        wp_register_style(
            'magnific',
            get_template_directory_uri() . '/assets/css/magnific-popup.css',
            array(),
            1,
            'all'
        );
        wp_enqueue_style('magnific');

        wp_register_style(
            'flaticon',
            get_template_directory_uri() . '/assets/css/flaticon.css',
            array(),
            1,
            'all'
        );
        wp_enqueue_style('flaticon');

        wp_register_style(
            'meanmenu',
            get_template_directory_uri() . '/assets/css/meanmenu.css',
            array(),
            1,
            'all'
        );
        wp_enqueue_style('meanmenu');

        wp_register_style(
            'style',
            get_template_directory_uri() . '/assets/css/style.css',
            array(),
            1,
            'all'
        );
        wp_enqueue_style('style');

        wp_register_style(
            'responsive',
            get_template_directory_uri() . '/assets/css/responsive.css',
            array(),
            1,
            'all'
        );
        wp_enqueue_style('responsive');
    }
    add_action('wp_enqueue_scripts', 'load_stylesheets');







    //load scripts

    function load_scripts()
    {
        wp_register_script(
            'jquery',
            get_template_directory_uri() . '/assets/js/jquery-2.2.4.min.js',
            array(),
            1,
            1,
            1
        );
        wp_enqueue_script('jquery');

        wp_register_script(
            'popper',
            get_template_directory_uri() . '/assets/js/popper.min.js',
            array(),
            1,
            1,
            1
        );
        wp_enqueue_script('popper');

        wp_register_script(
            'bootstrap',
            get_template_directory_uri() . '/assets/js/bootstrap.min.js',
            array(),
            1,
            1,
            1
        );
        wp_enqueue_script('bootstrap');

        wp_register_script(
            'carousel',
            get_template_directory_uri() . '/assets/js/owl.carousel.min.js',
            array(),
            1,
            1,
            1
        );
        wp_enqueue_script('carousel');

        wp_register_script(
            'waypoint',
            get_template_directory_uri() . '/assets/js/waypoint.min.js',
            array(),
            1,
            1,
            1
        );
        wp_enqueue_script('waypoint');


        wp_register_script(
            'counter',
            get_template_directory_uri() . '/assets/js/counter.min.js',
            array(),
            1,
            1,
            1
        );
        wp_enqueue_script('counter');

        wp_register_script(
            'magnificpopup',
            get_template_directory_uri() . '/assets/js/jquery.magnific-popup.min.js',
            array(),
            1,
            1,
            1
        );
        wp_enqueue_script('magnificpopup');

        wp_register_script(
            'meanmenu',
            get_template_directory_uri() . '/assets/js/meanmenu.js',
            array(),
            1,
            1,
            1
        );
        wp_enqueue_script('meanmenu');

        wp_register_script(
            'ajaxchimp',
            get_template_directory_uri() . '/assets/js/jquery.ajaxchimp.min.js',
            array(),
            1,
            1,
            1
        );
        wp_enqueue_script('ajaxchimp');

        wp_register_script(
            'form-validator',
            get_template_directory_uri() . '/assets/js/form-validator.min.js',
            array(),
            1,
            1,
            1
        );
        wp_enqueue_script('form-validator');

        wp_register_script(
            'form-validator',
            get_template_directory_uri() . '/assets/js/form-validator.min.js',
            array(),
            1,
            1,
            1
        );
        wp_enqueue_script('form-validator');

        wp_register_script(
            'slick',
            get_template_directory_uri() . '/assets/js/slick.min.js',
            array(),
            1,
            1,
            1
        );
        wp_enqueue_script('slick');

        wp_register_script(
            'custom',
            get_template_directory_uri() . '/assets/js/custom.js',
            array(),
            1,
            1,
            1
        );
        wp_enqueue_script('custom');
    }

    add_action('wp_enqueue_scripts', 'load_scripts');
    ?>